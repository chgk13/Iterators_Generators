from os import listdir
from os import stat
from os import getcwd


def hardlink_check(directory_path: str) -> bool:
    try:
        full_path = getcwd() + directory_path
        dir = listdir(full_path)
        print(dir)
    except FileNotFoundError:
        return False
    unique_inodes = set()
    for file_name in dir:
        inode = stat(full_path + '/' + file_name).st_ino
        if inode in unique_inodes:
            return True
        else:
            unique_inodes.add(inode)
    else:
        return False
