from collections import deque


class Graph:
    def __init__(self, E):
        self.E = E

    def __getitem__(self, item):
        return self.E[item]

    def set_of_vert(self):
        return set(self.E)


class GraphIterator:
    def __init__(self, graph, start_v):
        self.graph = graph
        self.start_v = start_v
        self.queue = deque()
        self.queue.append(self.start_v)
        self.visited = set()
        self.not_visited = graph.set_of_vert()

    def __iter__(self):
        return self

    def hasNext(self) -> bool:
        return bool(self.queue) or bool(self.not_visited)

    def __next__(self) -> str:
        if self.hasNext():
            vertex = (
                self.queue.popleft() if self.queue else self.not_visited.pop()
            )
            self.visited.add(vertex)
            self.not_visited.discard(vertex)
            for near in self.graph[vertex]:
                if near not in self.visited and near not in self.queue:
                    self.queue.append(near)
            return vertex
        raise StopIteration


def test():
    E = {"A": ["B", "C"], "B": ["A", "C", "D", "E"],
         "C": ["A", "B", "E"], "D": ["B"], "E": ["B", "C"],
         "F": ["G"], "G": ["F"]}
    start_v = "A"
    G = Graph(E)
    GI = GraphIterator(G, start_v)
    for vertex in GI:
        print(vertex)


test()
