def convert_to_rpn(input_string: str) -> str:
    stack = []
    result = []
    for char in input_string:
        if char.isalpha():
            result.append(char)
        elif char == '(':
            stack.append(char)
        elif char == ')':
            oper = stack.pop()
            while oper != '(':
                result.append(oper)
                oper = stack.pop()
        elif char in '+-':
            if len(stack) > 0 and stack[-1] != '(':
                result.append(stack.pop())
            stack.append(char)
        elif char in '*/':
            if len(stack) > 0 and stack[-1] in '*/':
                result.append(stack.pop())
            stack.append(char)
    while len(stack) > 0:
        result.append(stack.pop())
    return ''.join(result)


def convert_from_rpn(input_string: str) -> str:
    stack = []
    for char in input_string:
        if char.isalpha():
            stack.append([char, 'single'])
        if char in '+-':
            a = stack.pop()
            b = stack.pop()
            if char == '-' and a[1] == 'low':
                a[0] = a[0] = '(' + a[0] + ')'
            stack.append([b[0] + char + a[0], 'low'])
        if char in '*/':
            a = stack.pop()
            b = stack.pop()
            if a[1] in 'low' or (char == '/' and a[1] == 'high'):
                a[0] = '(' + a[0] + ')'
            if b[1] == 'low':
                b[0] = '(' + b[0] + ')'
            stack.append([b[0] + char + a[0], 'high'])
    return stack.pop()[0]


def brackets_trim(input_data: str) -> str:
    rpn = convert_to_rpn(input_data)
    return convert_from_rpn(rpn)


def test():
    print(brackets_trim('(a*(b/c)+((d-f)/(k*l)))'))
    print(brackets_trim('a'))
    print(brackets_trim('a-(b+c)'))


if __name__ == '__main__':
    test()
