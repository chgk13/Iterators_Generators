import os
import tempfile


def merge_files(fname1: str, fname2: str):
    try:
        file_1 = open(fname1)
    except IOError as e:
        raise OSError('Could not open {}'.format(e.filename))
    try:
        file_2 = open(fname2)
    except IOError as e:
        raise Exception('Could not open {}'.format(e.filename))
    try:
        num_2 = int(next(file_2))
    except StopIteration:
        num_2 = float('inf')
    res = tempfile.NamedTemporaryFile(
        mode='w', delete=False, prefix='res_', suffix='.txt', dir=os.getcwd())
    name = res.name.split('/').pop()
    for num_1 in file_1:
        num_1 = int(num_1)
        while num_2 < num_1:
            res.write(str(num_2) + '\n')
            try:
                num_2 = int(next(file_2))
            except StopIteration:
                num_2 = float('inf')
        res.write(str(num_1) + '\n')
    if num_2 != float('inf'):
        res.write(str(num_2) + '\n')
        for num_2 in file_2:
            num_2 = int(num_2)
            res.write(str(num_2) + '\n')
    file_1.close()
    file_2.close()
    res.close()
    return name
